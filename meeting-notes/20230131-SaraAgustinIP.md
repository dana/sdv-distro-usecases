# Agustin & Sara on Distro licensing and IP
--------------------

## Date: 2023-01-31 

## Agenda:
- SDV [Distro Scope](https://gitlab.eclipse.org/eclipse-wg/sdv-wg/sdv-technical-alignment/sdv-technical-topics/sdv-distro-usecases/-/blob/main/README.md)
- Using [Leda](https://projects.eclipse.org/projects/automotive.leda) as basis: Linux distribution based on Yocto Project

Note: 
Leda already has [releases](https://eclipse-leda.github.io/leda/docs/general-usage/download-releases/ ), with the note: There are no official releases yet. The artifacts available on the Release page are for testing the build and release workflows and should be considered as unstable nightly builds from the main branch.

- SDV Projects integration in Leda
- Synergies with Oniro 


## Notes:
First Oniro release in Dec. License compliance work was done.
License compliance team with dedicated tooling and system engineers and lawyers - dedicated staff and knowledge in the team. Process and tools were validated internally at the EF by hiring a consultant company, via an audit. Tools, SW were open source, they did not release binaries. Binaries cannot be released because it has proprietary code to run on a board (eg raspberry Pi). Oniro provides instructions for how to build Oniro for RasperryPi. 
Risk 5 is for small devices, risk 5 architecture is open source but not necessarily the drivers. 
1. release without binaries - SW packages 
2. build license compliance process 
3. in future release binaries including SW close to the HW 

Proposal from Agustin: share IP team and infrastructure, otherwise EF will not be able to take of IP for OS. It has always been the plan for Oniro to support SDV. Some internal decisions from EF should be taken for the infrastructure migration. 

**Material IP toolchain**

- [Migration plan](https://gitlab.eclipse.org/eclipse-wg/oniro-wg/products-services-oniro-wg/it-services-oniro-wg/coordination-it-services-oniro-wg/-/blob/main/oniro-it-infrastructure-and-services-implementation-and-migration-plan.md)
- Videos
   - [Why and high level description](https://www.youtube.com/watch?v=aNLbtzu6ZHk&list=PLy7t4z5SYNaQBDReZmeHAknEchYmu0LLa&index=38)
   - Talk at FOSSnorth 2022. Overview of the work done by the [Oniro IP team](https://www.youtube.com/watch?v=a_jJRe4nN-U)

   Notes 
   
   Tools
     - Fossology and ScanCode
      - debian matching: provides machine readable copyrights and license information. automates most of work on third party packages
     - Gitlab CI pipeline 
     - Alien4Friends - Eclipse Oniro Compliance Toolchain
: [open source project](https://projects.eclipse.org/projects/oniro.oniro-compliancetoolchain) Continuous Compliance workflow for Oniro repositories.
     - SPDX Software Package Data Exchange format to express SW bill of material (SBOM)
     - REUSE: some packages are REUSE compliant allor for decicated fossology agent ojo 

   How to use the tools in a Toolchain:
   1. provide SW packages to Fossology with suport of ScanCode
   2. look for matching debian packages 
   3. use Fossology agent for REUSE compliant packages 
   4. compile software bill of material (SBOM) in SPDX format
   5. audit work 

   - Talk at the [Yocto project](https://www.youtube.com/watch?v=czWPD0aiMEM)


**Actions**

- [x] Sara to FW the invitation of breakout group to Agustin
- [ ] Sara to talk to Wayne & Rahul (licence compliance eng) to get the background story from EF side 

**Open questions**

**Do we really need a compliance toolchain in SDV??**

